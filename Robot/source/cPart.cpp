/*
 David Alexis Zarate Trujillo A01332978
 Eduardo Valencia Paz A01333346
 Cesar David Betancourt Adame A01338883
 David Guillermo Legorreta Barba A01335334
 Julio Miguel. A. Enriquez Castillo A01335065
 */

#include "../header/cPart.hpp"

Part::Part(float _x, float _y, float _z, float _width, float _height, float _length, float _r, float _g, float _b) {
    x = _x;
    y = _y;
    z = _z;
    width = _width;
    height = _height;
    length = _length;
    
    r = _r;
    g = _g;
    b = _b;
}

Part::Part(){
    
}

Part::~Part() {
    
}

void Part::draw() {
    glColor3f(r, g, b);
    
    // Draw front side bottom-left, bottom-right, top-right, top-left, respectively
    glBegin(GL_POLYGON);
    {
        glVertex3f(x, y, z);
        glVertex3f(x+width, y, z);
        glVertex3f(x+width, y+height, z);
        glVertex3f(x, y+height, z);
    }
    glEnd();
    
    // Draw back side bottom-left, bottom-right, top-right, top-left, respectively
    glBegin(GL_POLYGON);
    {
        glVertex3f(x, y, z-length);
        glVertex3f(x+width, y, z-length);
        glVertex3f(x+width, y+height, z-length);
        glVertex3f(x, y+height, z-length);
    }
    glEnd();
    
    // Draw right side bottom-left, bottom-right, top-right, top-left, respectively
    glBegin(GL_POLYGON);
    {
        glVertex3f(x+width, y, z);
        glVertex3f(x+width, y, z-length);
        glVertex3f(x+width, y+height, z-length);
        glVertex3f(x+width, y+height, z);
    }
    glEnd();
    
    // Draw left side bottom-left, bottom-right, top-right, top-left, respectively
    glBegin(GL_POLYGON);
    {
        glVertex3f(x, y, z-length);
        glVertex3f(x, y, z);
        glVertex3f(x, y+height, z);
        glVertex3f(x, y+height, z-length);
    }
    glEnd();
    
    // Draw top side bottom-left, bottom-right, top-right, top-left, respectively
    glBegin(GL_POLYGON);
    {
        glVertex3f(x, y+height, z);
        glVertex3f(x+width, y+height, z);
        glVertex3f(x+width, y+height, z-length);
        glVertex3f(x, y+height, z-length);
    }
    glEnd();
    
    // Draw bottom side bottom-left, bottom-right, top-right, top-left, respectively
    glBegin(GL_POLYGON);
    {
        glVertex3f(x, y, z);
        glVertex3f(x+width, y, z);
        glVertex3f(x+width, y, z-length);
        glVertex3f(x, y, z-length);
    }
    glEnd();
}

void Part::update() {
    
}
