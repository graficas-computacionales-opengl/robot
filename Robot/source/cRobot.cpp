/*
 David Alexis Zarate Trujillo A01332978
 Eduardo Valencia Paz A01333346
 Cesar David Betancourt Adame A01338883
 David Guillermo Legorreta Barba A01335334
 Julio Miguel. A. Enriquez Castillo A01335065
 */

#include "../header/cRobot.hpp"

Robot::Robot() {
    
    jump = 0.0f;
    jumpDirection = 1.0f;
    
    for(int i = 0;  i < ROBOTPARTS; i++){
        parts[i] = new Part();
        rotX[i] = 0.0f;
        rotY[i] = 0.0f;
        rotZ[i] = 0.0f;
        rotXDirection[i] = 1.0f;
        rotYDirection[i] = 1.0f;
        rotZDirection[i] = 1.0f;
    }

    
    // configure each part if needed
    // Part coordinates are the bottom left coordinates of the cube, then it draws it counter
    // clock-wise, first front and the back
    // Part(float _x, float _y, float _z, float _width, float _height, float _length, float _r, float _g, float _b)
    
    // Main Body
    parts[BODY]      = new Part(-1.0f, -0.5f, 0.5f, 2.0f, 0.8f, 1.0f, 0.9f, 0.9f, 0.9f);
    parts[CHEST]     = new Part(-1.25f, 0.3f, 0.625f, 2.5f, 1.5f, 1.25f, 1.0f, 0.0f, 0.0f);
    parts[NECK]      = new Part(-0.25f, 1.8f, 0.25f, 0.5f, 0.5f, 0.5f, 1.0f, 1.0f, 1.0f);
    parts[HEAD]      = new Part(-0.425f, 2.1f, 0.425f, 0.85f, 0.85f, 0.85f, 0.0f, 0.0f, 1.0f);
    parts[HIP]       = new Part(-1.125f, -1.1f, 0.6f, 2.25f, 0.6f, 1.2f, 0.7f, 0.7f, 0.7f);
    
    // Left arm
    /*
     parts[LSHOULDER] = new Part(-2.05f, 1.2f, 0.4f, 0.8f, 0.5f, 0.8f, 1.0f, 1.0f, 1.0f);
     parts[LARM]      = new Part(-1.95f, 1.7f, 0.35f, 0.6f, 0.7f, 0.7f, 1.0f, 0.0f, 0.0f);
     parts[LFOREARM]  = new Part(-2.05f, 2.4f, 0.4f, 0.8f, 1.0f, 0.8f, 1.0f, 1.0f, 1.0f);
     parts[LHAND]     = new Part(-1.95f, 3.4f, 0.35f, 0.6f, 0.5f, 0.7f, 0.0f, 0.0f, 1.0f);
     */
    parts[LSHOULDER] = new Part(-2.05f, 1.2f, 0.4f, 0.8f, 0.5f, 0.8f, 1.0f, 1.0f, 1.0f);
    parts[LARM]      = new Part(-1.95f, 1.7f, 0.35f, 0.6f, 0.7f, 0.7f, 1.0f, 0.0f, 0.0f);
    parts[LFOREARM]  = new Part(-2.05f, 2.35f, 0.35f, 0.8f, 1.0f, 0.8f, 1.0f, 1.0f, 1.0f);
    parts[LHAND]     = new Part(-1.95f, 3.35f, 0.3f, 0.6f, 0.5f, 0.7f, 0.0f, 0.0f, 1.0f);
    
    // Right arm
    /*
     parts[RSHOULDER] = new Part(1.25f, 1.2f, 0.4f, 0.8f, 0.5f, 0.8f, 1.0f, 1.0f, 1.0f);
     parts[RARM]      = new Part(1.35f, 1.7f, 0.35f, 0.6f, 0.7f, 0.7f, 1.0f, 0.0f, 0.0f);
     parts[RFOREARM]  = new Part(1.25f, 2.4f, 0.4f, 0.8f, 1.0f, 0.8f, 1.0f, 1.0f, 1.0f);
     parts[RHAND]     = new Part(1.35f, 3.4f, 0.35f, 0.6f, 0.5f, 0.7f, 0.0f, 0.0f, 1.0f);
     */
    parts[RSHOULDER] = new Part(1.25f, 1.2f, 0.4f, 0.8f, 0.5f, 0.8f, 1.0f, 1.0f, 1.0f);
    parts[RARM]      = new Part(1.35f, 1.7f, 0.35f, 0.6f, 0.7f, 0.7f, 1.0f, 0.0f, 0.0f);
    parts[RFOREARM]  = new Part(1.25f, 2.35f, 0.35f, 0.8f, 1.0f, 0.8f, 1.0f, 1.0f, 1.0f);
    parts[RHAND]     = new Part(1.35f, 3.35f, 0.3f, 0.6f, 0.5f, 0.7f, 0.0f, 0.0f, 1.0f);
    
    // Left leg
    parts[LTHIGH]    = new Part(-1.2f, -2.3f, 0.5f, 0.8f, 1.2f, 1.0f, 1.0f, 1.0f, 1.0f);
    parts[LLEG]      = new Part(-1.25f, -3.7f, 0.55f, 0.9f, 1.4f, 1.1f, 0.0f, 0.0f, 1.0f);
    parts[LFOOT]     = new Part(-1.25f, -4.2f, 1.25f, 0.9f, 0.5f, 1.8f, 1.0f, 0.0f, 0.0f);
    
    // Right leg
    parts[RTHIGH]    = new Part(0.4f, -2.3f, 0.5f, 0.8f, 1.2f, 1.0f, 1.0f, 1.0f, 1.0f);
    parts[RLEG]      = new Part(0.35f, -3.7f, 0.55f, 0.9f, 1.4f, 1.1f, 0.0f, 0.0f, 1.0f);
    parts[RFOOT]     = new Part(0.35f, -4.2f, 1.25f, 0.9f, 0.5f, 1.8f, 1.0f, 0.0f, 0.0f);
    
    // Rotations
    rotX[RSHOULDER] = 180;
    rotXDirection[RSHOULDER] = 1.0f;
    
    rotX[LSHOULDER] = 180;
    rotXDirection[LSHOULDER] = -1.0f;
    
    rotX[RFOREARM] = -25.0f;
    rotX[LFOREARM] = -25.0f;
    
    rotXDirection[RTHIGH] = 1.0f;
    rotXDirection[LTHIGH] = -1.0f;
    
    rotXDirection[RLEG] = 1.0f;
    rotXDirection[LLEG] = 1.0f;
    startLeftLegRot = false;
}

Robot::~Robot() {
    // delete [] parts;
    // delete rotX;
    // delete rotY;
    // delete rotZ;
}

void Robot::draw() {
    glPushMatrix();
    {
        glTranslatef(0.0f, jump, 0.0f);
        parts[BODY]->draw();
        parts[NECK]->draw();
        parts[HEAD]->draw();
        parts[HIP]->draw();
        glPushMatrix(); // Start upper Body movement
        {
            glRotatef(rotY[CHEST], 0, 1, 0);
            parts[CHEST]->draw();
            
            glPushMatrix(); // Start all right arm movement
            {
                glTranslatef(
                             (parts[RSHOULDER]->x)-((parts[RSHOULDER]->width)/2.0f),
                             (parts[RSHOULDER]->y)+((parts[RSHOULDER]->height)/2.0f),
                             (parts[RSHOULDER]->z)-((parts[RSHOULDER]->length)/2.0f));
                glRotatef(rotX[RSHOULDER], 1, 0, 0);
                glTranslatef(
                             -(parts[RSHOULDER]->x)+((parts[RSHOULDER]->width)/2.0f),
                             -(parts[RSHOULDER]->y)-((parts[RSHOULDER]->height)/2.0f),
                             -(parts[RSHOULDER]->z)+((parts[RSHOULDER]->length)/2.0f));
                parts[RSHOULDER]->draw();
                parts[RARM]->draw();
                glPushMatrix(); // Start right forearm and hand movement
                {
                    glTranslatef(
                                 (parts[RFOREARM]->x)-((parts[RFOREARM]->width)/2.0f),
                                 (parts[RFOREARM]->y)+((parts[RFOREARM]->height)/2.0f),
                                 (parts[RFOREARM]->z)-((parts[RFOREARM]->length)/2.0f));
                    glRotatef(rotX[RFOREARM], 1, 0, 0);
                    glTranslatef(
                                 -(parts[RFOREARM]->x)+((parts[RFOREARM]->width)/2.0f),
                                 -(parts[RFOREARM]->y)-((parts[RFOREARM]->height)/2.0f),
                                 -(parts[RFOREARM]->z)+((parts[RFOREARM]->length)/2.0f));
                    parts[RFOREARM]->draw();
                    parts[RHAND]->draw();
                }
                glPopMatrix(); // Finish right forearm and hand movement
            }
            glPopMatrix(); // Start all right arm movement
            
            glPushMatrix(); // Start all left arm movement
            {
                glTranslatef(
                             (parts[LSHOULDER]->x)-((parts[LSHOULDER]->width)/2.0f),
                             (parts[LSHOULDER]->y)+((parts[LSHOULDER]->height)/2.0f),
                             (parts[LSHOULDER]->z)-((parts[LSHOULDER]->length)/2.0f));
                glRotatef(rotX[LSHOULDER], 1, 0, 0);
                glTranslatef(
                             -(parts[LSHOULDER]->x)+((parts[LSHOULDER]->width)/2.0f),
                             -(parts[LSHOULDER]->y)-((parts[LSHOULDER]->height)/2.0f),
                             -(parts[LSHOULDER]->z)+((parts[LSHOULDER]->length)/2.0f));
                parts[LSHOULDER]->draw();
                parts[LARM]->draw();
                
                glPushMatrix(); // Start left forearm and hand movement
                {
                    glTranslatef(
                                 (parts[LFOREARM]->x)-((parts[LFOREARM]->width)/2.0f),
                                 (parts[LFOREARM]->y)+((parts[LFOREARM]->height)/2.0f),
                                 (parts[LFOREARM]->z)-((parts[LFOREARM]->length)/2.0f));
                    glRotatef(rotX[LFOREARM], 1, 0, 0);
                    glTranslatef(
                                 -(parts[LFOREARM]->x)+((parts[LFOREARM]->width)/2.0f),
                                 -(parts[LFOREARM]->y)-((parts[LFOREARM]->height)/2.0f),
                                 -(parts[LFOREARM]->z)+((parts[LFOREARM]->length)/2.0f));
                    parts[LFOREARM]->draw();
                    parts[LHAND]->draw();
                }
                glPopMatrix(); // Finish left forearm and hand movement
            }
            glPopMatrix(); // Finish all left arm movement
        }
        glPopMatrix(); // Finish upper Body movement
        
        glPushMatrix(); // Start all right leg movement
        {
            glTranslatef(
                         (parts[RTHIGH]->x)+((parts[RTHIGH]->width)/2.0f),
                         (parts[RTHIGH]->y)+(parts[RTHIGH]->height),
                         (parts[RTHIGH]->z)-((parts[RTHIGH]->length)/2.0f));
            glRotatef(rotX[RTHIGH], 1, 0, 0);
            glTranslatef(
                         -(parts[RTHIGH]->x)-((parts[RTHIGH]->width)/2.0f),
                         -(parts[RTHIGH]->y)-(parts[RTHIGH]->height),
                         -(parts[RTHIGH]->z)+((parts[RTHIGH]->length)/2.0f));
            parts[RTHIGH]->draw();
            
            glPushMatrix(); // Start right leg and foot movement
            {
                glTranslatef(
                             (parts[RLEG]->x)+((parts[RLEG]->width)/2.0f),
                             (parts[RLEG]->y)+(parts[RLEG]->height),
                             (parts[RLEG]->z)-((parts[RLEG]->length)/2.0f));
                glRotatef(rotX[RLEG], 1, 0, 0);
                glTranslatef(
                             -(parts[RLEG]->x)-((parts[RLEG]->width)/2.0f),
                             -(parts[RLEG]->y)-(parts[RLEG]->height),
                             -(parts[RLEG]->z)+((parts[RLEG]->length)/2.0f));
                parts[RLEG]->draw();
                parts[RFOOT]->draw();
            }
            glPopMatrix(); // Finish right leg and foot movement
        }
        glPopMatrix(); // Finish all right leg movement
        
        glPushMatrix(); // Start all left leg movement
        {
            glTranslatef(
                         (parts[LTHIGH]->x)+((parts[LTHIGH]->width)/2.0f),
                         (parts[LTHIGH]->y)+(parts[LTHIGH]->height),
                         (parts[LTHIGH]->z)-((parts[LTHIGH]->length)/2.0f));
            glRotatef(rotX[LTHIGH], 1, 0, 0);
            glTranslatef(
                         -(parts[LTHIGH]->x)-((parts[LTHIGH]->width)/2.0f),
                         -(parts[LTHIGH]->y)-(parts[LTHIGH]->height),
                         -(parts[LTHIGH]->z)+((parts[LTHIGH]->length)/2.0f));
            parts[LTHIGH]->draw();
            
            glPushMatrix(); // Start left leg and foot movement
            {
                glTranslatef(
                             (parts[LLEG]->x)+((parts[LLEG]->width)/2.0f),
                             (parts[LLEG]->y)+(parts[LLEG]->height),
                             (parts[LLEG]->z)-((parts[LLEG]->length)/2.0f));
                glRotatef(rotX[LLEG], 1, 0, 0);
                glTranslatef(
                             -(parts[LLEG]->x)-((parts[LLEG]->width)/2.0f),
                             -(parts[LLEG]->y)-(parts[LLEG]->height),
                             -(parts[LLEG]->z)+((parts[LLEG]->length)/2.0f));
                parts[LLEG]->draw();
                parts[LFOOT]->draw();
            }
            glPopMatrix(); // Finish left leg and foot movement
        }
        glPopMatrix(); // Finish all left leg movement
    }
    glPopMatrix();
}

void Robot::update() {
    if(jumpDirection > 0.0f) {
        if(jump <= 0.125f) {
            jump += 0.0142857f;
        } else {
            jumpDirection = -1.0f;
        }
    } else {
        if(jump >= -0.125f) {
            jump -= 0.0142857f;
        } else {
            jumpDirection = 1.0f;
        }
    }
    
    
    // Should change position in 17.5 frames
    
    // Chest rotation conditions
    if(rotYDirection[CHEST] > 0){
        if(rotY[CHEST] <= 7.0f) {
            rotY[CHEST] += 0.4f;
        } else {
            rotYDirection[CHEST] = -1.0f;
        }
    } else {
        if(rotY[CHEST] >= -7.0f) {
            rotY[CHEST] -= 0.4f;
        } else {
            rotYDirection[CHEST] = 1.0f;
        }
    }
    
    // All right arm rotation conditions
    if(rotXDirection[RSHOULDER] > 0){
        if(rotX[RSHOULDER] <= 195.0f) {
            rotX[RSHOULDER] += 0.857142f;
        } else {
            rotXDirection[RSHOULDER] = -1.0f;
        }
    } else {
        if(rotX[RSHOULDER] >= 165.0f) {
            rotX[RSHOULDER] -= 0.857142f;
        } else {
            rotXDirection[RSHOULDER] = 1.0f;
        }
    }
    
    // All left arm rotation conditions
    if(rotXDirection[LSHOULDER] > 0){
        if(rotX[LSHOULDER] <= 195.0f) {
            rotX[LSHOULDER] += 0.857142;
        } else {
            rotXDirection[LSHOULDER] = -1.0f;
        }
    } else {
        if(rotX[LSHOULDER] >= 165.0f) {
            rotX[LSHOULDER] -= 0.857142;
        } else {
            rotXDirection[LSHOULDER] = 1.0f;
        }
    }
    
    // All right leg rotation conditions
    if(rotXDirection[RTHIGH] > 0){
        if(rotX[RTHIGH] <= 20.0f) {
            rotX[RTHIGH] += 1.1428565;
            rotX[RLEG] = 0.0f; // Related to leg bottom part
        } else {
            rotXDirection[RTHIGH] = -1.0f;
        }
    } else {
        if(rotX[RTHIGH] >= -20.0f) {
            rotX[RTHIGH] -= 1.1428565;
            // Related to leg bottom part
            if(rotX[RTHIGH] >= 0.0f){
                rotX[RLEG] += 3.5f;
            } else {
                rotX[RLEG] -= 3.0f;
            }
        } else {
            rotXDirection[RTHIGH] = 1.0f;
        }
    }
    
    // All left leg rotation conditions
    if(rotXDirection[LTHIGH] > 0){
        if(rotX[LTHIGH] <= 20.0f) {
            rotX[LTHIGH] += 1.1428565;
            rotX[LLEG] = 0.0f; // Related to leg bottom part
        } else {
            rotXDirection[LTHIGH] = -1.0f;
        }
    } else {
        if(rotX[LTHIGH] >= -20.0f) {
            rotX[LTHIGH] -= 1.1428565;
            // Related to leg bottom part
            if(rotX[LTHIGH] >= 0.0f){
                rotX[LLEG] += 3.5f;
                startLeftLegRot = true;
            } else {
                if(startLeftLegRot){
                    rotX[LLEG] -= 3.0f;
                }
            }
        } else {
            rotXDirection[LTHIGH] = 1.0f;
        }
    }
}
