/*
 David Alexis Zarate Trujillo A01332978
 Eduardo Valencia Paz A01333346
 Cesar David Betancourt Adame A01338883
 David Guillermo Legorreta Barba A01335334
 Julio Miguel. A. Enriquez Castillo A01335065
 */

#pragma one

#ifdef __APPLE__
// See: http://lnx.cx/docs/opengl-in-xcode/
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#include <GLUT/glut.h>
#else
#include "freeglut.h"/Users/davidzarate/Documents/Tec/ITC/Semestre8/GraficasComputacionales/Parcial1/Actividades/Robot/Robot/header/cPart.hpp
#endif

#include <stdio.h>
#include <math.h>

class Part {
public:
    Part();
    Part(float _x, float _y, float _z, float _width, float _height, float _length, float _r, float _g, float _b);
    ~Part();
    
    void draw();
    void update();
    
    float x;
    float y;
    float z;
    
    float width;
    float height;
    float length;
    
    float r;
    float g;
    float b;
};
