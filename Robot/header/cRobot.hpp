/*
 David Alexis Zarate Trujillo A01332978
 Eduardo Valencia Paz A01333346
 Cesar David Betancourt Adame A01338883
 David Guillermo Legorreta Barba A01335334
 Julio Miguel. A. Enriquez Castillo A01335065
 */

#pragma one

#ifdef __APPLE__
// See: http://lnx.cx/docs/opengl-in-xcode/
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#include <GLUT/glut.h>
#else
#include "freeglut.h"
#endif

#include <stdio.h>
#include <math.h>

#include "./cPart.hpp"

#ifndef ROBOTPARTS
    #define ROBOTPARTS 21
#endif

class Robot {
public:
    Robot();
    ~Robot();
    
    void draw();
    void update();
    
    Part *parts[ROBOTPARTS];
    float rotX[ROBOTPARTS];
    float rotY[ROBOTPARTS];
    float rotZ[ROBOTPARTS];
    float rotXDirection[ROBOTPARTS];
    float rotYDirection[ROBOTPARTS];
    float rotZDirection[ROBOTPARTS];
    
    bool startLeftLegRot;
    float jump;
    float jumpDirection;
    
    enum RobotParts {
        BODY,
        CHEST,
        NECK,
        HEAD,
        HIP,
        LSHOULDER,
        LFOREARM,
        LELBOW,
        LARM,
        LHAND,
        LTHIGH,
        LLEG,
        LFOOT,
        RSHOULDER,
        RFOREARM,
        RELBOW,
        RARM,
        RHAND,
        RTHIGH,
        RLEG,
        RFOOT
    };
};
